#include "includes.h"

// A pipeline stage consist of a thread executing its routine, a condition to notify it, a mutex to synchronize it, a process function (corresponding to the work it does on a sentence) and its thread
struct PipelineStage
{
    std::thread thread;
    std::queue<std::string*> toProcess;
    std::condition_variable condition;
    std::mutex mutex;
    void (*processFunction)(std::string*);
};

class PipelineNLP {
public:
    std::vector<std::string> Process(std::vector<std::string> sentences);

private:
    // The stages of the pipeline
    PipelineStage stages[4];

    // The routine of a pipeline stage, aka, the behaviour it has concerning waiting to be notified, processing a sentence, and notifying the next one
    static void PipelinePartRoutine(PipelineStage* part, PipelineStage* nextPart, unsigned int total);

    // The function used by the
    static void LowerCaseStep(std::string* sentence);
    static void Tokenize(std::string* sentence);
    static void DeletePunctuation(std::string* sentence);
    static void NumbersToLetters(std::string* sentence);
};
#include "includes.h"

class ThreadPool {
    using Job = std::function<void()>;

public:
    ThreadPool(const unsigned int& threadCount);
    void Add(Job job);
    void Start();
    void WaitEnd();
private:
    std::mutex mutex;
    std::vector<std::thread> threads;
    std::queue<Job> jobs; // List of jobs
};
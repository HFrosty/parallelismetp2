#include "Cracker.h"

std::string Cracker::Crack(std::string crypted) {
    bool done = false;
    std::string decrypted;
    auto bruteforce = [&done, &decrypted](std::string crypted, char firstletter) {
        for(char c0 = 'a'; c0 <= 'z'; ++c0) {
            for(char c1 = 'a'; c1 <= 'z'; ++c1) {
                for(char c2 = 'a'; c2 <= 'z'; ++c2) {
                    for(char c3 = 'a'; c3 <= 'z'; ++c3) {
                        if(done) {
                            return;
                        }
                        std::string word = std::string() + firstletter + c0 + c1 + c2 + c3;
                        if(md5(word) == crypted) {
                            done = true;
                            decrypted = word;
                        }
                    }
                }
            }
        }
    };

    std::thread threads[26]; // One thread for each letter of the alphabet
    for(unsigned int i = 0; i < 26; ++i) {
        threads[i] = std::thread(bruteforce, crypted, 'a' + i);
    }

    for(unsigned int i = 0; i < 26; ++i) {
        if(threads[i].joinable()) {
            threads[i].join();
        }
    }

    return decrypted;
}
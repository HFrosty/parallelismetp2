# M1 Parallèlisme - TP2

## Prérequis

- CMake (version 3.17+)
- Système d'exploitation Linux
- g++ (avec capacité à compiler du C++20)

## Démarches

1. Se placer dans le dossier bin
2. Build le projet cmake -> Commande : `cmake ../`
3. Build le projet -> Commande : `cmake --build .`
4. Démarrer le programme -> Commande : `./ParallelismeTP2`
#include "ThreadPool.h"

ThreadPool::ThreadPool(const unsigned int& threadCount) {
    this->threads.resize(threadCount);
}

void ThreadPool::Add(Job job) {
    this->jobs.push(job);
}

void ThreadPool::Start() {
    auto threadRoutine = [this]() {
        while(jobs.size() != 0) { // If no jobs are left, the thread end
            // We lock the mutex, so another thread doesn't modify the queue while we're using it
            mutex.lock();
            if(jobs.empty()) { // If it turns out the jobs are empty after another thread released the lock (and emptied the queue), the thread work is done
                mutex.unlock();
                return;
            }
            auto job = jobs.front();
            jobs.pop();
            // The job has been took, we don't modify the queue anymore, so we allow the other threads waiting to modify the queue
            mutex.unlock();
            // Calculations
            job();
        }
    };

    for(unsigned int i = 0; i < this->threads.size(); ++i) {
        this->threads[i] = std::thread(threadRoutine);
    }
}

void ThreadPool::WaitEnd() {
    for(unsigned int i = 0; i < this->threads.size(); ++i) {
        if(this->threads[i].joinable()) {
            this->threads[i].join();
        }
    }
    return;
}
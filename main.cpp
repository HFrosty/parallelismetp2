#include "includes.h"
#include "ThreadPool.h"
#include "PipelineNLP.h"
#include "Cracker.h"

// The function given as a task to the thread pool
void JobFunction(float& result, unsigned int i, std::mutex* mutex) {
    double factorial = 1.0f;
    for(unsigned int j = i; j > 0; --j) {
        factorial = factorial * j;
    }
    
    mutex->lock(); // We lock so that 2 threads do not write at the same time on the same variable
    result += std::pow(-1.0f, i) / factorial; // The result is added to the float reference
    mutex->unlock(); // Unlock so waiting threads can add
}

void Exercice1() {
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "----------------------------\t\tExercice1\t\t----------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;

    constexpr unsigned int N = 5000;
    float result = 0.0f;
    std::mutex mutex;
    ThreadPool pool = ThreadPool(std::thread::hardware_concurrency());
    for(unsigned int i = 0; i < N; ++i) {
        // We use std::bind to transform our function that needs two arguments to one that needs none
        pool.Add(std::bind(JobFunction, std::ref(result), i, &mutex));
    }
    pool.Start();
    pool.WaitEnd();
    std::cout << "Result: " << result << std::endl;
}

void Exercice2() {
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "----------------------------\t\tExercice2\t\t----------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;

    std::vector<std::string> text = {
        "Coucou, j'ai le numero 2.",
        "Salutation : le numero 7 vous parle!",
        "Suis-je vraiment le numero 4 ?",
        "Sens de la vie : 42"
    };

    PipelineNLP pipeline;
    std::vector<std::string> processed = pipeline.Process(text);
    for(unsigned int i = 0; i < text.size(); ++i) {
        std::cout << "-------------------\nInput:\t" << text[i] << "\nOutput:\t" << processed[i] << std::endl;
    }
}

void Exercice3() {
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "----------------------------\t\tExercice3\t\t----------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;

    std::string secret = "3ed7dceaf266cafef032b9d5db224717";
    Cracker cracker;
    std::string decrypt = cracker.Crack(secret);
    std::cout << "Input:\t" << secret << "\nOutput:\t" << decrypt << std::endl;
}

int main() {
    Exercice1();
    Exercice2();
    Exercice3();
    return 0;
}
#include "PipelineNLP.h"

std::vector<std::string> PipelineNLP::Process(std::vector<std::string> sentences) {
    // We give an id to each stage of the pipeline, to make the code easier to understand
    constexpr unsigned int LOWERCASE = 0;
    constexpr unsigned int TOKEN = 1;
    constexpr unsigned int PUNCTUATION = 2;
    constexpr unsigned int NUMBERS = 3;

    unsigned int size = sentences.size();

    // We tell to each stage, which function is the one it has to use to process a sentence
    stages[LOWERCASE].processFunction = LowerCaseStep;
    stages[TOKEN].processFunction = Tokenize;
    stages[PUNCTUATION].processFunction = DeletePunctuation;
    stages[NUMBERS].processFunction = NumbersToLetters;
    // We start the threads by telling them : on which stage they operate, the stage that comes after the stage they operate, and the number of sentences they will have to process
    stages[LOWERCASE].thread = std::thread(PipelinePartRoutine, &stages[LOWERCASE], &stages[TOKEN], size);
    stages[TOKEN].thread = std::thread(PipelinePartRoutine, &stages[TOKEN], &stages[PUNCTUATION], size);
    stages[PUNCTUATION].thread = std::thread(PipelinePartRoutine, &stages[PUNCTUATION], &stages[NUMBERS], size);
    stages[NUMBERS].thread = std::thread(PipelinePartRoutine, &stages[NUMBERS], nullptr, size);

    // Put sentences in queue
    for(unsigned int i = 0; i < sentences.size(); ++i) {
        // For each sentence, we tell the lowercase stage it has to process them and notify it if it is currently waiting
        auto lowerLock = std::unique_lock<std::mutex>(stages[LOWERCASE].mutex);
        stages[LOWERCASE].toProcess.push(&sentences[i]);
        stages[LOWERCASE].condition.notify_one();
    }

    // We wait for the stages to end
    stages[LOWERCASE].thread.join();
    stages[TOKEN].thread.join();
    stages[PUNCTUATION].thread.join();
    stages[NUMBERS].thread.join();

    return sentences;
}

void PipelineNLP::PipelinePartRoutine(PipelineStage* stage, PipelineStage* nextStage, unsigned int total) {
    // We leave the thread it has processed as much sentences as they are in total
    unsigned int count = 0;
    while(count != total) {
        // We create a lock to wait for a notification that should come from the previous stage
        auto lock = std::unique_lock<std::mutex>(stage->mutex);
        stage->condition.wait(lock, [stage]{
            return stage->toProcess.size() > 0;
            });
        // We take the first sentence of the sentences waiting to be processed by the current stage and remove it from the queue
        std::string* sentence = stage->toProcess.front();
        stage->toProcess.pop();
        // We process the sentence
        stage->processFunction(sentence);
        // If we are not the last stage, we add the sentence we processed to the to-do list of the next stage and notifies it if it was waiting
        if(nextStage != nullptr) {
            auto nextLock = std::unique_lock<std::mutex>(nextStage->mutex);
            nextStage->toProcess.push(sentence);
            nextStage->condition.notify_one();
        }
        ++count;
    }
}

void PipelineNLP::LowerCaseStep(std::string* sentence) {
    for(unsigned int i = 0; i < sentence->size(); ++i) {
        (*sentence)[i] = std::tolower((*sentence)[i]);
    }
}

void PipelineNLP::Tokenize(std::string* sentence) {
    const std::string tokenElement[] = {"\'"};
    for(auto element : tokenElement) {
        if(sentence->find(element) != std::string::npos) {
            sentence->replace(sentence->begin() + sentence->find(element), sentence->begin() + sentence->find(element)+element.length(), element + " ");
        }
    }
}

void PipelineNLP::DeletePunctuation(std::string* sentence) {
    const std::string punctuation[] = {":", ",", ".", ";", "?", "!"};
    for(auto element : punctuation) {
        if(sentence->find(element) != std::string::npos) {
            sentence->replace(sentence->begin() + sentence->find(element), sentence->begin() + sentence->find(element)+element.length(), "");
        }
    }
}

void PipelineNLP::NumbersToLetters(std::string* sentence) {
    std::map<std::string, std::string> conversion = {
        {"1", "un"},
        {"2", "deux"},
        {"3", "trois"},
        {"4", "quatre"},
        {"5", "cinq"},
        {"6", "six"},
        {"7", "sept"},
        {"8", "huit"},
        {"9", "neuf"},
    };
    auto iterator = conversion.begin();
    while(iterator != conversion.end()) {
        if(sentence->find(iterator->first) != std::string::npos) {
            sentence->replace(sentence->begin() + sentence->find(iterator->first), sentence->begin() + sentence->find(iterator->first) + iterator->first.length(), iterator->second);
        }
        ++iterator;
    }
}